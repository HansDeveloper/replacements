// ==UserScript==
// @name        xkcd-substitutions
// @namespace   hans
// @description SUBSTITUTIONS That make reading the news more "fun". (http://xkcd.com/1288/)
// @updateURL   https://gitlab.com/HansDeveloper/replacements/raw/master/xkcdHans.user.js
// @downloadURL https://gitlab.com/HansDeveloper/replacements/raw/master/xkcdHans.user.js
// @include     *bbc.co.uk/*
// @include     *bbc.com/*
// @include     *cbc.ca/*
// @include     *theguardian.com/*
// @include     *telegraph.co.uk/*
// @include     *theonion.com/*
// @include     *foxnews.com/*
// @include     *wikipedia.org/*
// @include     *merkur-online.de
// @include     *computerbase.de/*
// @include     *facebook.com/*
// @include     *geeksaresexy.net/*
// @include     *golem.de/*
// @include     *gulli.com/*
// @include     *heise.de/*
// @include     *minecraft.gamepedia.com/*
// @include     *merkur.de/*
// @include     *spiegel.de/*
// @include     *sueddeutsche.de/*
// @include     *zeit.de/*
// @include     *ze.tt/*
// @include     *bento.de/*
// @include     *bigpanda.cern.ch/*
// @include     *bibleserver.com/*
// @include     *wikisource.org/*
// @include     *news.ycombinator.com/*
// @include     *brusselstimes.com/*
// @include     file:///tmp/test.html
// @version     1.1.59
// @grant       none
// ==/UserScript==

(function() {
    var substitutions;
    var textNodes;
    var regexps = {};
    var swapregexps = {};
    var backswapregexps = {};

    substitutions = {
    'hitler': 'hotler',
    'witnesses': 'these dudes I know',
    'allegedgly': 'kinda probably',
    'new study': 'Tumblr post',
    'rebuild': 'avenge',
    'space': 'spaaace',
    'google glass': 'Virtual Boy',
    'smartphone': 'Pokédex',
    'senator': 'elf-lord',
    'car': 'cat',
    'election': 'eating contest',
    'congressional leaders': 'river spirits',
    'homeland security': 'homestar runner',
    'could not be reached for comment': 'is guilty and everyone knows it',
    'ebola': 'Legolas',
    'russland': 'deine Mutter',
    'minecraft': 'mein Kraft',
    'Barack Obama': 'Darth Vader',
    'Obama': 'Vader',
    'George W. Bush': 'George "Wicked" Bush',
    'Schottland': 'Schrottland',
    'feminismus': 'lila Hut',
    'million': 'melon',
    'physik': 'physik, das muss man wissen',
    'medizin': 'medizin, das ist Mathematik, Physik',
    'biologie': 'biologie, das ist Physik',
    'ministerium' : 'hansdb97531',
    'ministerien' : '83497543niars',
    'minister': 'penis',
    'mysterium' : 'ministerium',
    'mysterien' : 'ministerien',
    'hansdb97531' : 'mysterium',
    '83497543niars' : 'mysterien',
    'de Maiziere': 'des Nightmare',
    'de Maizière': 'des Nightmare',
    'facebook': 'facefuck',
    'apple': 'banana',
    'polizei': 'prinzengarde',
    'papst': 'breznsepp',
    'päpst': 'breznsepp',
    'atom(en|e|)': 'rosinenkuchen',
    'ele(c|k)tr': 'atom',
    'magnet': 'bird',
    'nabla': 'blabla',
    '(\\. |\\! |\\? |: )No(\\.|\\!)': '$1Computer says nooo$2',
    'Theresa May': 'Theresa "Fancypants" May',
    'selfie': 'fucking selfie',
    'patienten': 'enten',
    'patient': 'ente',
    'quanten': 'knödel',
    'deutschland': 'schland',
    'wissenschaftler': 'vergewaltiger',
    'internet': 'neuland',
    'force': 'horse',
    'russian': 'your mom\'s',
    'russia': 'your mom',
    'wasser': 'gulaschsaft',
    'kanzler': 'hitler',
    'fu(ß|ss)ball': 'hallenhalma',
    'polizist': 'prinzengardist',
    'chef': 'häuptling',
    'maut': 'nutte',
    'diplom': 'jodeldiplom',
    'abitur': 'jodeldiplom',
    'drohne': 'reichsflugscheibe',
    'virus': 'sonderangebot',
    'bayreuth': 'Wo? Fraglos nur ein Ort: Bayreuth!',
    'microsoft': 'mordor',
    'laser': '"laser""',
    'scientist': 'rapist',
    'prozent': 'bier',
    '%': ' Bier',
    'promille': 'bierchen',
    '‰': ' Bierchen',
    'gepanzert': 'gebollert',
    'panzer': 'bollerwagen',
    'kommissar': 'troll',
    'partei': 'tippgemeinschaft',
    'waffe': 'waffel',
    'iphone': 'banana phone',
    'putin': 'pudding',
    'Sigmar Gabriel': 'Sigmar "Siggi Pop" Gabriel',
    'Hans-Werner Sinn': 'Hans-Werner "Un"Sinn',
    'milliarde': 'marmelade',
    'Spender' : 'Beleidiger',
    '(\\s)spenden(\\s)' : '$1beleidigen$1',
    'spenden([a-zA-Z])' : 'beleidigungs$1',
    'spenden' : 'beleidigungen',
    'gespendet' : 'beleidigt',
    'spendiert' : 'beleidigt',
    'spendieren' : 'beleidigen',
    'spende': 'beleidigung',
    'Angela Merkel': 'Angie',
    'Kramp-Karrenbauer' : 'Kramp-Knarrenbauer-Kohlwerfer-Königfeind-Kaiserfreund-Kleehauser-Kraxenhuber-Krünberger-Karolinger-Kastenrieder-Korbschneider-Krausenmüller-Keinschmied-Schmidt',
    'Friedrich Merz': 'Blackrock Biedermeier',
    'merz': 'biedermeier',
    'Günther Oettinger': 'Doktor Oettker',
    'Oettinger': 'Oettker',
    'terror': 'tenor',
    'islam': 'salam',
    'winter': 'dreckswinter is coming',
    'George R. R. Martin': 'George harr harr Martin',
    'nobelpreis': 'popelpreis',
    'nobel prize': 'no bell prize',
    'charlie Hebdo': 'mich',
    'window': 'door',
    'nuclear': 'nucular',
    'sparta': 'this is sparta',
    'cloud': 'claus',
    'Charles( Robert)* Darwin': 'Charles "Jerusalem" Darwin',
    'mi(c|k)ro': 'ma$1ro',
    'raster': 'rasta',
    'kohl': 'birne',
    'erzbischof': 'erdbeerschorsch',
    'weihbischof': 'weißbierschorsch',
    'erzbischöfe': 'erdberschorschs',
    'weihbischöfe': 'weißbierschorschs',
    'Margot Käßmann': 'Margot "1,54‰" Käßmann',
    'Joachim Gauck': 'Joachim "Hipster" Gauck',
    'Günter Grass': 'Günter "Anders" Grass',
    'broken' : 'screwed',
    'aborted' : 'ceased to be',
    'submitting' : 'walking',
    'data15' : 'oldshit',
    'data16' : 'newshit',
    'CPU-hrs' : 'toy money',
    'hilfe': 'mayonnaise',
    'nuklear': 'nukular',
    'sehr' : 'anders',
    'magie': 'physik durch Wollen',
    'Philipp Rösler': 'Philipp "Fipsi" Rösler',
    'Guido Westerwelle': 'Guido "hat euch lieb" Westerwelle',
    'Jürgen Trittin': 'Jürgen "Tritt Ihn"',
    'Karl-Theodor Maria Nikolaus Johann Jacob Philipp Franz Joseph Sylvester Buhl-Freiherr von und zu Guttenberg': 'Karl-Theodor Maria Nikolaus Johann Jacob Philipp Franz Joseph Sylvester Neujahr Heiligdreikönig Buhl-Freiherr von und zu Guttenberg ',
    'Karl-Theodor zu Guttenberg': 'Karl-Theodor "es lohnt sich" zu Guttenberg',
    'Frank-Walter Steinmeier': 'Frank-Walter "MC" Steinmeier',
    'Silvio Berlusconi': 'Silvio "Bunga Bunga" Berlusconi',
    'Horst Seehofer': 'Horsti Seehofer',
    'debate': 'dance off',
    'candidate': 'airbender',
    'drone': 'dog',
    'vows to': 'probably won\'t',
    'at large': 'very large',
    'successfully': 'suddenly',
    'expands': 'physically expands',
    'an unknown number': 'like hundreds',
    'front runner': 'blade runner',
    'global': 'spherical',
    'no indication': 'lots of signs',
    'horsepower': 'tons of horsemeat',
    'Theo Waigel': 'Theo "the eyebrow" Waigel',
    'von der Leyen': '"Panzeruschi" von der siebenfachen alten Leier',
    '(Ilse )*Aigner': 'Ilse',
    'Stoiber': '"Ede" Stoiber',
    'Brandt': 'Zwieback',
    'kraft': 'mondelēz',
    'CSU': 'CSD',
    'CDU': 'PDU',
    'SPD': 'Anonyme Masochisten',
    'katholi': 'koreani',
    'Tebartz': 'Teebatz',
    'Christian': 'Farbian',
    'evangeli': 'epilepti',
    'chris(t|)': 'pflanz',
    'kilo': 'killer',
    'mega': 'meger',
    'giga': 'tiger',
    'tera': 'terror',
    'peta': 'peter',
    's([iy])lvester': 'sl$1vester',
    'politi': 'choleri',
    'airplane': 'fool\'s planet',
    'super bowl': 'superb owl',
    'super': 'süper',
    'kultur': 'kültür',
    'ultra': 'ültra',
    'Trump': 'Drumpf',
    'crispr': 'lecker knusper',
    'waschmaschine': 'schnitzelzentrifuge',
    'washing machine': 'schnitzel centrifuge',
    '\! ': '\! OH\!',
    'flüchtling': 'schwammerling',
    'wein': 'wife',
    'wine': 'wife',
    'Stephen Bannon': 'Stephen "LL" Bannon',
    'Mike Flynn': 'Mike Flynn ("Drei-Sterne-General")',
    'Mike Pompeo': 'Pompeii Mike',
    'Reince Priebus': 'Rebus Nicer-Pie',
    'Priebus': 'Nicer-Pie',
    'Jeff Sessions': 'Jam Sessions',
    'Myron Ebell': 'Myon Rebell',
    'Ebell': 'Rebell',
    'Rudy Giuliani': 'Rudy "Gaudily I ruin NY" Giuliani',
    'Jared Kushner': 'Jared "horse whisperer" Kushner',
    'James Mattis': 'Mad Dog',
    'Steven Mnuchin': 'Steven Munchkin',
    'Mnuchin': 'Munchkin',
    'Sarah Palin': 'Sarah "I can see your mom" Palin',
    'Michael Rogers': 'Mr. Rogers',
    'Mitt Romney': 'Mit oder Ohne Romney', // Suggestion of zeit online 21.11.2016
    'Rex Tillerson': 'Tyrannosaurus Rex',
    'Zinke': 'der Zinker',
    'Sonny Perdue': 'Rain Prayer',
    'Andy Puzder': 'An die Puszta',
    'Andrew Puzder': 'An die Puszta',
    'Tom Price': 'Top Rice',
    'Elaine Chao': 'E-lane Ciao',
    'Rick Perry': 'Katy Perry',
    'Betsy DeVos': 'Betsy derWos ins Hirn gschissen ham',
    'David Shulkin': '',
    'John F. Kelly': 'John Family Kelly',
    'Mike Pence': 'Non-Sense',
    'Wilbur Ross': 'Bilbo Rosskur',
    'Jürgen Klinsmann': 'Klinsi',
    'Klinsmann': 'Klinsi',
    'Butterwegge': 'Buttersemmeln',
    'Helmut Schmidt': 'Smoky Schmidt',
    'Ludwig Erhard': 'Erhard Ludwig',
    'Martin Schulz': 'Gottkanzler Schulz',
    'Frauke Petry': 'Frau Kepetry',
    'Hoeneß': '"Steuern? Nicht mit mir!" Hoeneß',
    'Marine Le Pen': 'Kugelschreibermarie',
    'Boris Johnson': 'Bojo the clown',
    'Sebastian Kurz': 'Babyhitler',
    'Strache': '"Ibiza" Strache',
    "Scheuer": "Bescheuert",
    "Schwesig": "Schleswig",
    "Zuckerberg": "Süßstoffberg",
    "Brüssel": "Brssl",
    'macron': 'macaroni',
    'boeing': 'boring',
    'militia': 'fanclub',
    'birnee': 'birnée',
    'krieg':'ketchup',
    '(^|[^a-zäöüA-ZÄÖÜ])war([^a-rt-zäöüA-RT-ZÄÖÜ]|$)':'$1ketchup$2',
    '(^|[^a-zäöüA-ZÄÖÜ])War([^a-rt-zäöüA-RT-ZÄÖÜ]|$)':'$1Ketchup$2',
    'zaun':'brücke',
    'zäune':'brücken',
    '(^|[^a-zäöüA-ZÄÖÜ])poll([^a-rt-zäöüA-RT-ZÄÖÜ]|$)':'$1psychic reading$2',
    '(^|[^a-zäöüA-ZÄÖÜ])Poll([^a-rt-zäöüA-RT-ZÄÖÜ]|$)':'$1Psychic reading$2',
    'ae':'ä',
    'oe':'ö',
    'ue':'ü',
    'jura' : 'jura, es lohnt sich',
    'violence' : 'violins',
    'Niederländer': 'Sumpfneger',
    'Niederland': 'Sumpfland',
    'Farbian Lindner': 'Farbian "dornige Chancen" Lindner',
    'NSA':'\u{1F4E1}',
    'National Security Agency':'\u{1F4E1}',
    'Kim Jong-un':'kim "Very much alive" Jong-un (\u{1F680})',
    'intelligent': 'intellent',
    'hans[a-z]+': 'hans',
    '(^|[^a-zäöüA-ZÄÖÜ])([A-Z]|Sch).{0,2}an[sz]' : '$1Hans',
    '(^|[^a-zäöüA-ZÄÖÜ])([a-z]|sch).{0,2}an[sz]' : '$1hans',
	'Olaf': 'Oil of',
	'Hubertus Heil': 'Hubertus Sieg Heil',
	'Spahn': 'den kannst dir sparn',
	'Heiko': 'Haiku',
	'Katarina Barley': 'Gerstenkathy (#mussreichen)',
	'hashtag': '#haschtag',
	'Söder': 'Kreuzritter Söder',
	'blau': 'superhansfarbhans123',
	'grün': 'superhansfarbhans124',
	'gelb': 'superhansfarbhans125',
	'schwarz': 'superhansfarbhans126',
	'braun': 'superhansfarbhans127',
	'(^|[^a-zäöüA-ZÄÖÜ])rot': '$1superhansfarbhans128',
	'(^|[^a-zäöüA-ZÄÖÜ])Rot': '$1superhansfarbhans130',
	'weiß': 'superhansfarbhans129',
	'superhansfarbhans123': 'grün',
	'superhansfarbhans124': 'gelb',
	'superhansfarbhans125': 'schwarz',
	'superhansfarbhans126': 'braun',
	'superhansfarbhans127': 'rot',
	'superhansfarbhans128': 'weiß',
	'superhansfarbhans130': 'Weiß',
	'superhansfarbhans129': 'blau',
	'gesicht': 'gsicht',
    'erd(e|)' : 'superhelmhans123',
    'luft($|[^a-zäöüA-ZÄÖÜ])' : 'erde$1',
    'luft([a-zäöüA-ZÄÖÜ])' : 'erd$1',
    'superhelmhans123' : 'luft',
    'jesu(s*)': 'jesushans123',
    'hodor': 'jesus',
    'jesushans123': 'hodor',
    'ost': 'osthans',
    'east': 'osthans',
    'west': 'ost',
    'osthans': 'west',
    'mops' : 'mopsi',
    'v\.[ \u{202F}\u{00A0}]?l\.[ \u{202F}\u{00A0}]?n\.[ \u{202F}\u{00A0}]?r': 'o. B. d. A',
    'f\.[ \u{202F}\u{00A0}]?l\.[ \u{202F}\u{00A0}]?t\.[ \u{202F}\u{00A0}]?r': 'w. l. o. g',
    'DIN': 'DINGSDABUMS',
    'ISO': 'SOWISO',
    'Judäa': 'Spalter',
    'Freund': 'Mellon',
    'twitter': 'die 8$ Klitsche',
    'Ron DeSantis': '',
    'Alfons': 'Aloisius',
    'Schuhbeck': 'Ingwersepp',
    'deep': 'derp'
    }

    swaps ={
        'anti': 'ali',
        'histo' : 'hyste',
        'krise': 'herausforderung',
        'gewebe': 'gewerbe',
        'years': 'minutes',
        'ortho': 'para',
        'link': 'recht',
        'oben': 'unten',
        'right': 'left',
        'defense': 'offense',
        'defence': 'offence',
        'verteidigung': 'angriff',
        'ice': 'fire',
        'unter': 'über',
        'fence': 'bridge',
        'mauer':'brücke',
        'putsch':'punsch',
        'sanit':'attent',
        'hetero': 'homo',
        'produktion': 'provokation',
        'production': 'provocation',
        'produce': 'provoke',
        'börse': 'korea',
        'produz': 'provoz',
        'hyper': 'hypo',
        'hippo': 'scooter',
        'jugendlich' : 'jüdisch',
        'jugend': 'juden',
        'nilpferd': 'roller',
        'amüsant' : 'relevant',
        'Bundestag' : 'Schützenverein',
        'kryptisch' : 'kritisch',
        'Gauland' : 'Gauleiter',
        'Mars' : 'Mond',
        'up' : 'down',
        'top+' : 'bottom',
        'over' : 'under',
        'online': 'offline',
        'anal': 'oral',
        'earth': 'air',
        'stern': 'spiegel',
        'kabinett': 'schrank',
        'schulz': 'scholz',
        'geist': 'schnaps',
        'nord': 'süd',
        'north': 'south',
	    'berg': 'fisch',
        'corona' : 'desperados',
        'Robert' : 'Roland',
        'Elon': 'Melon',
        'Musk': 'Tusk',
        'china': 'porzellan',
        'Xi Jinping': 'Pu der Bär',
        'tesla': 'edison'
    
    }

    sometimesSubstitutions = [
        // [regex, subst, probability]
        ['(^|[^a-zäöüA-ZÄÖÜ])(der|die|das|den|dem)([^a-zäöüA-ZÄÖÜ]|$)', '$1Harry Potter (\u{26A1}) und $2 $3', 0.05],
        ['(^|[^a-zäöüA-ZÄÖÜ])the([^a-zäöüA-ZÄÖÜ]|$)', '$1Harry Potter (\u{26A1}) and the $2', 0.05],
        // ['(^|[^a-zäöüA-ZÄÖÜß])[A-ZÄÖÜ][a-zäöüA-ZÄÖÜß]{3,}', '$1Hans', 0.05],
        ['\\.(\\s)', '. Und Gott sah, dass es gut war.$1', 0.05],
        ['\\?', '\? Was\? OB DU BEHINDERT BIST HAB ICH DICH GEFRAGT\?!\?', 0.05]
    ]

    var tmpsub = {}
    // Add capitalized versions to map
    // Put lower and uppercase immediately after each other (hoping
    // that JS actually reads the dict later this order). This helps
    // sometimes avoiding successive replacements.
    for (var key in substitutions){
        tmpsub[key] = substitutions[key];
        tmpsub[key.charAt(0).toUpperCase() + key.slice(1)] = substitutions[key].charAt(0).toUpperCase() + substitutions[key].slice(1);
    }
    substitutions = tmpsub;

    var tmpsometimessub = []
    for (var i in sometimesSubstitutions) {
        tmpsometimessub.push(sometimesSubstitutions[i]);
        var firstLetterRegex = sometimesSubstitutions[i][0].charAt(0).toUpperCase()
        var firstLetterSubst = sometimesSubstitutions[i][1].charAt(0).toUpperCase()
        if (firstLetterRegex.toLowerCase() != firstLetterRegex &&
            firstLetterSubst.toLowerCase() != firstLetterSubst) {
            // append capitalized versions if first symbol is a letter
            tmpsometimessub.push([
                sometimesSubstitutions[i][0].charAt(0).toUpperCase() + sometimesSubstitutions[i][0].slice(1),
                sometimesSubstitutions[i][1].charAt(0).toUpperCase() + sometimesSubstitutions[i][1].slice(1),
                sometimesSubstitutions[i][2]
            ]);
        }
    }
    sometimesSubstitutions = tmpsometimessub

    for (var key in swaps){
        swaps[key.charAt(0).toUpperCase() + key.slice(1)] = swaps[key].charAt(0).toUpperCase() + swaps[key].slice(1);
    }

    for (var key in substitutions) {
        regexps[key] = new RegExp(key ,'g');
    }
    for (var key in swaps) {
        swapregexps[key] = new RegExp(key ,'g');
        backswapregexps[swaps[key]] = new RegExp(swaps[key] ,'g');
    }
    for (var i in sometimesSubstitutions) {
        sometimesSubstitutions[i][0] = new RegExp(sometimesSubstitutions[i][0], 'g');
    }

    var t0 = performance.now();
    textNodes = document.evaluate("//text()", document, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
    var totaltime_normal = 0;
    var totaltime_sometimes = 0;
    var totaltime_swap = 0;
    var totaltime_randQuotes = 0;
    var t0_sub = 0;
    var t1_sub = 0;
    for (var i = 0; i < textNodes.snapshotLength; i++) {
        var node = textNodes.snapshotItem(i);
        t0_sub = performance.now();
        node.data = substituteTextIn(node.data);
        t1_sub = performance.now();
        totaltime_normal += t1_sub - t0_sub;
        t0_sub = performance.now();
        node.data = substituteTextInSometimes(node.data);
        t1_sub = performance.now();
        totaltime_sometimes += t1_sub - t0_sub;
        t0_sub = performance.now();
        node.data = swapTextIn(node.data);
        t1_sub = performance.now();
        totaltime_swap += t1_sub - t0_sub;
        t0_sub = performance.now();
        node.data = randomQuotes(node.data);
        t1_sub = performance.now();
        totaltime_randQuotes += t1_sub - t0_sub;
    }
    var t1 = performance.now();
    console.log("Call xkcdHans took " + (t1 - t0) + " milliseconds.");
    console.log("Normal substitutions " + totaltime_normal + " milliseconds.");
    console.log("Sometimes substitutions " + totaltime_sometimes + " milliseconds.");
    console.log("Swap substitutions " + totaltime_swap + " milliseconds.");
    console.log("Random quote substitutions " + totaltime_randQuotes + " milliseconds.");

    function substituteTextIn(text){
        for (var key in substitutions) {
            text = text.replace(regexps[key], substitutions[key]);
        }
        return text;
    }

    function substituteTextInSometimes(text){
        for (var i in sometimesSubstitutions) {
            text = replaceSometimes(text,
                                    sometimesSubstitutions[i][0],
                                    sometimesSubstitutions[i][1],
                                    sometimesSubstitutions[i][2]);
        }
        return text;
    }

    function swapTextIn(text){
        for (var key in swaps) {
            text = text.replace(swapregexps[key], 'superhans12342321');
            text = text.replace(backswapregexps[swaps[key]], removeRegexChars(key));
            var regex = new RegExp('superhans12342321', 'g');
            text = text.replace(regex, removeRegexChars(swaps[key]));
        }
        return text;
    }

    function replaceRandomly(textList){
        var newList = [];
        for (var i in textList){
            var regex = new RegExp('([^\.,:\"\']+)', 'g');
            var wordMatch = regex.exec(textList[i]);
            var relativeProbability = 0.;
            if (wordMatch) {
                /*
                  Turn-on curve for probability - for fine tuning try this in gnuplot:
                  maxprob=0.1
                  scaling=0.3
                  threshold=10
                  set xr [0:30]
                  set yr [0:0.1]
                  plot 0.5*maxprob*(tanh(scaling*(x-threshold))+1)
                 */
                var maxprob = 0.1; // maximum probability
                var scaling = 0.3; // scale the "turn-on" - smaller value means slower turn on
                var threshold = 10; // threshold where 0.5*maxprob is reached
                var relativeProbability = 0.5*maxprob*(Math.tanh(scaling*(wordMatch[0].length-threshold))+1)
            }
            if (Math.random() > (1-relativeProbability)) {
                newList.push(textList[i].replace(regex, '"$1"'));
            }
            else{
                newList.push(textList[i]);
            }
        }
        return newList;
    }

    function randomQuotes(text){
        var spaceMatch = new RegExp('\\s+', 'g');
        var split = text.split(spaceMatch);
        text = replaceRandomly(split).join(' ');

        return text;

    }

    function replaceSometimes(text, regex, substitution, probability){
        // replace the given regex by the given substitution only with a certain probability
        var match = 1;
        var nextPiece = text;
        var newText = "";
        while (match) {
            var nextMatchPos = nextPiece.search(regex);
            if (nextMatchPos < 0) {
                newText += nextPiece;
                break;
            }
            newText += nextPiece.substr(0, nextMatchPos);
            nextPiece = nextPiece.slice(nextMatchPos);
            match = nextPiece.match(regex);
            var stuffToReplace = nextPiece.substr(0, match[0].length);
            nextPiece = nextPiece.slice(match[0].length);
            if (Math.random() < probability) {
                newText += stuffToReplace.replace(regex, substitution);
            } else {
                newText += stuffToReplace;
            }
        }
        return newText;
    }

    function removeRegexChars(text) {
      // adapt depending on what will be needed
      return text.replace("+", "").replace("*", "").replace(".", "")
    }
        
})();
