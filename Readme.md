# Version number bump commit hook

To avoid forgetting to bump the version number of `xkcdHans.user.js` install the pre-commit hook:

```
ln -s ../../pre-commit .git/hooks
```
